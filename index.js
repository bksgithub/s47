const txt_first_name = document.querySelector("#txt-first-name");
const txt_last_name = document.querySelector("#txt-last-name");
let span_full_name = document.querySelector("#span-full-name");


txt_first_name.addEventListener("keyup", () => {
fullName()
})

txt_last_name.addEventListener("keyup", () => {
	fullName()
})

function fullName() {
	span_full_name.innerHTML = txt_first_name.value + " " + txt_last_name.value
}


txt_first_name.addEventListener("keyup", (event) => {
	console.log(event.target);
	console.log(event.target.value);
})


//event.target is just the same as the current element. Which means the code below will also work the same way.
// txt_first_name.addEventListener("keyup", () => {
// 	console.log(txt_first_name);
// 	console.log(txt_first_name.value);
// })